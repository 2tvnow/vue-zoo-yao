import Vue from 'vue'
import Vuex from 'vuex'
import originalDataSet from './resourses/animalsDataList.json'

Vue.use(Vuex)

export default new Vuex.Store({

  state: {
    animals: []
  },

  mutations: {
    DELETE_ITEM (state, payload) {
      const index = this.state.animals.indexOf(payload);
      state.animals.splice(index, 1)
    },

    DELETE_ITEMS (state,payload) {
      payload.items.map((item) => {
        const index = this.state.animals.indexOf(item);
        state.animals.splice(index, 1)
      })
    },

    SET_EDITED_ITEM (state, payload) {
      Object.assign(state.animals[payload.index], payload.item);
    },

    ADD_NEW_ITEM (state, payload) {
      state.animals.push(payload)
    },

    REMOVE_ALL_ITEMS () {
      return new Promise((resolve) => {
        this.state.animals = []
        resolve('cleanData successfully')
      })
    },

    SET_DEFAULT_DATASET () {
      return new Promise((resolve) => {
        originalDataSet.map((item) => {
          this.state.animals.push({
              age: item.age,
              type: item.type,
              weight: item.weight,
              name:item.name
          })
        })
        resolve('generateDataset successfully')
      })
    }//end of generateDataset();
  },

  actions: {
    async initialize (context) {
      await context.commit('REMOVE_ALL_ITEMS')
      await context.commit('SET_DEFAULT_DATASET')
    },

    deleteItem (context, item) {
      context.commit('DELETE_ITEM', item)
    },

    deleteItems (context, item) {
      context.commit('DELETE_ITEMS', item)
    },

    setEditedItem (context, payload) {
      context.commit('SET_EDITED_ITEM', payload)
    },

    addNewItem (context, payload) {
      context.commit('ADD_NEW_ITEM', payload)
    }
  },

  getters: {
    getAnimalsRawData (state) {
      return state.animals
    }
  }
})
