import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Zoo from './views/Zoo.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/zoo',
      name: 'zoo',
      component: Zoo
    }
  ]
})
