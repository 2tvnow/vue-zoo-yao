export default {
  headers: [
    {
      text: 'Name',
      value: 'name'
    },
    {
      text: 'Type',
      value: 'type'
    },
    {
      text: 'Age',
      value: 'age'
    },
    {
      text: 'Weight (kg)',
      value: 'weight'
    },
    {
      text: 'Actions',
      sortable: false
    }
  ],
  defaultItemDataSet: {
    name: '',
    type: '',
    age: '',
    weight: ''
  }
}
